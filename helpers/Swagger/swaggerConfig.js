/**
 * @file Initiate and configure swagger
 * @author Sahan R Thathsara
 */
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Album apis',
            version: '1.0.0',
            description: 'simple gapstars album project'
        },
        servers: [
            {
                url: 'http://localhost:4060'
            }
        ]
    },
    apis: ['./routes/*.js']
};
const swaggerSpecs = swaggerJsdoc(options);

function swaggerInit(app) {
    app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));
}

export default { swaggerInit };
