/**
 * @file Mongo db connector and initiation
 * @author Sahan Ridma
 */
import mongoose from 'mongoose';

function init() {
    try {
        mongoose.connect(
            // eslint-disable-next-line no-undef
            config.MONGO_URL,
            {
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            },
            (error) => {
                if (error) {
                    console.log('Error on connecting to MongoDB', error);
                } else {
                    console.log('Connected to MongoDB | Ready for use.');
                }
            }
        );
    } catch (error) {
        console.log(error);
    }
}

export default { init };
