/**
 * @file Uses to inject esm for the application
 * @author Sahan Ridma
 */
const requireESM = require('esm')(module);

const app = requireESM('../app');

const port = process.env.PORT || 4060;

app.listen(port, () => {
    const { name: appName, version: appVersion } = require('../package.json');
    console.log(appName, appVersion, 'is running on ', port);
});
