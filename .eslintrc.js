module.exports = {
    env: {
        browser: true,
        es2021: true
    },
    extends: ['airbnb-base', 'prettier'],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module'
    },
    rules: {
        indent: [2, 4, { SwitchCase: 1 }],
        'import/no-extraneous-dependencies': [
            'error',
            { devDependencies: true }
        ],
        'operator-linebreak': [
            'error',
            'after',
            { overrides: { ':': 'before', '?': 'before' } }
        ],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'no-unused-vars': ['error', { args: 'none' }],
        'max-len': ['error', 150],
        'no-console': 0,
        'prefer-promise-reject-errors': ['error', { allowEmptyReject: true }],
        'class-methods-use-this': 'off',
        'import/named': 'off',
        'func-names': 'off',
        'newline-per-chained-call': ['error', { ignoreChainWithDepth: 6 }],
        'global-require': 0,
        'default-case': 0,
        'no-underscore-dangle': ['error', { allow: ['_id'] }],
        'consistent-return': 0
    }
};
