import 'regenerator-runtime/runtime';
import { test, expect, beforeAll } from '@jest/globals';
import mongoose from 'mongoose';
import albumService from '../../services/album';

/**
 * Here the mongo url is hard coded only in this case.
 * Should be moved to separate test config file/setup file for db connection and re-use it in other test file
 * @description This is only to demonstrate the nodejs Test scenario
 */
beforeAll(async () => {
    const url =
        'mongodb+srv://gapstarDemo:zKxSTdmBbWkCSz0U@personaldemo.7f5ux.mongodb.net/gapstarTest?retryWrites=true&w=majority';
    await mongoose.connect(url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    });
});
let albumId = null;
test('should create a new album', async (done) => {
    const payload = {
        sortedList: [{ id: 1, index: 0 }],
        ownerId: 1,
        title: 'test-album'
    };
    const response = await albumService.createAlbum(payload);
    albumId = response._id;
    expect(response.sortedList[0].id).toEqual(1);
    done();
}, 3000);

test('should update albums sorted array', async (done) => {
    await albumService.updateSortedList({
        albumId,
        sortedList: [{ id: 1, index: 1 }],
        ownerId: 1
    });
    const response = await albumService.getAlbum({ ownerId: 1, albumId });
    expect(response.sortedList[0].index).toBe(1);
    done();
}, 3000);

test('should get an album', async (done) => {
    const response = await albumService.getAlbum({ ownerId: 1, albumId });
    expect(response.sortedList[0].id).toEqual(1);
    done();
}, 3000);

test('should delete the album', async (done) => {
    const response = await albumService.deleteAlbum({ ownerId: 1, albumId });
    expect(response.sortedList[0].id).toEqual(1);
    done();
}, 3000);
