## Introduction
> This project is only developed for the purpose of Gapstar assignment. 
  This project does not include any authentication mechanism.
  This service only saves sorted image values and retrieve them.  

## Installation

Initially clone the project from this repository.

```sh
$ https://gitlab.com/gapstars-demo/image-sort-api.git
```
Create .env file from default.env. replace mongo url for testing
 ```sh
 $ mongodb+srv://gapstarDemo:zKxSTdmBbWkCSz0U@personaldemo.7f5ux.mongodb.net/gapstar?retryWrites=true&w=majority
 ``` 
Please refer following steps to run the project in local or development
environment. First clone the project form this repository then run following
commands.

```sh
$ npm install
$ npm start
```


## Code/Folder Structure

This application contains following code/folder structure.

-   `bootloaders/` – booting logic based on the environment
-   `bootloaders/express.js` – booting logic for express - Development purpose
    Production
-   `controllers/` – defines API routes' logic
-   `helpers/` – project specific all the reusable code logics(Utils)
    interceptors
-   `routes/` – contains the express router specific to the each
    end-point(Grouped based on the bushiness purpose)
-   `schemas/` – provide validation rules for each API request
-   `services/` – contain all the controller related main business logics
-   `app.js` – initializes the app and glues everything together
-   `package.json` – remembers all packages that your app depends on and their
    versions
-   `tests` – similar folder structure for all test files 

#### Developer Testing

Please execute following command for run unit tests.

```sh
$ npm test
```

#### Docker Build
- navigate to the root directory and run 
```sh
$ docker build -t gapstar-api .
```
