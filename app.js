import cors from 'cors';
import express from 'express';
import dotEnv from 'dotenv';
import mongoDB from './helpers/database-connectors/mongo-connector';
import albumRoutes from './routes/album';
import swagger from './helpers/Swagger/swaggerConfig';

global.config = dotEnv.config().parsed;

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
mongoDB.init();
app.use(cors());
/**
 * Initiate swagger for api documentation
 */
swagger.swaggerInit(app);

/**
 * manRoutes is the basic route handling file. all the application routes included in that file,
 * @description /gapstar is kind of a gateway prefix if needed to expand in future
 */
app.use('/albums', albumRoutes);

module.exports = app;
