FROM node:erbium-alpine

WORKDIR /user/src/app
COPY . .

RUN apk add --no-cache git

RUN npm install

EXPOSE 4060

CMD ["npm", "start"]
