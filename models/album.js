import { Schema, model } from 'mongoose';
import sortedImage from './sub/sortedList';

const schema = new Schema(
    {
        ownerId: {
            type: Number,
            required: true
        },
        sortedList: [sortedImage.schema], // uses sub document to store array of image list
        title: { type: String, default: 'untitled' }
    },
    { collation: 'albums', timestamps: true }
);

export default model('Album', schema);
