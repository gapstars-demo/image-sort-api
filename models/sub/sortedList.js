import { Schema, model } from 'mongoose';

const schema = new Schema(
    {
        id: {
            type: Number,
            required: true
        },
        index: {
            type: Number,
            required: true
        }
    },
    { collation: 'sorted_list', timestamps: true }
);

export default model('SortedImage', schema);
