/**
 * @file Uses to validate each request
 * @author Sahan Ridma
 * @description this file only uses to validate sorting controller related requests.
 */
import { Joi } from 'celebrate';
import JoiObjectId from 'joi-objectid';

Joi.objectId = JoiObjectId(Joi);

export default {
    createAlbum: {
        body: {
            ownerId: Joi.number().optional(),
            sortedList: Joi.array().items(
                Joi.object({
                    id: Joi.number().required(),
                    index: Joi.number().required()
                }).required()
            ),
            title: Joi.string().optional()
        }
    },

    updatePhotoOrder: {
        body: {
            ownerId: Joi.number().optional(),
            sortedList: Joi.array().items(
                Joi.object({
                    id: Joi.number().required(),
                    index: Joi.number().required()
                }).required()
            ),
            albumId: Joi.objectId().required()
        }
    },

    deleteAlbum: {
        params: {
            albumId: Joi.objectId().required()
        }
    },
    getAlbum: {
        params: {
            albumId: Joi.objectId().required()
        }
    }
};
