/**
 * @file handles all the sorting related routing
 * @author Sahan Ridma
 */
import express from 'express';
import { celebrate } from 'celebrate';
import SortingController from '../controllers/album';
import albumSchema from '../schemas/album';

const sortingCtrl = new SortingController();
const router = express.Router();

function logger(req, res, next) {
    console.log(req.method, req.url);
    next();
}
router.use(logger);
router.get('/health', (req, res) =>
    res.status(200).json('demo gapstar service is running')
);

/**
 * @swagger
 * components:
 *  schemas:
 *   Album:
 *    type: object
 *    description: user can create and maintain their own photo albums
 *    properties:
 *     _id:
 *      type: string
 *      example: objectId
 *     ownerId:
 *      type: number
 *     title:
 *      type: string 
 *     sortedList:
 *      type: array
 *      items:
 *       type: object
 *       properties:
 *        id:
 *         type: number
 *        index:
 *         type: number
 *       description: best 9 photos are stored with their respective order as index
 *
 */

/**
 * @swagger
 * /album/{albumId}:
 *  get:
 *   summary: Returns an album
 *   parameters:
 *    - name: albumId
 *      in: path
 *      required: true
 *      type: ObjectId
 *      description: album id is system generated Id by mongodb
 *   responses:
 *    '200':
 *     description: OK
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         message:
 *          type: string
 *          example: successfully fetched
 *         data:
 *          type: array
 *          items:
 *           type: object
 *           example: {'sortedList':[{'id':1231, 'index':0},{'id':213, 'index':1}], 'ownerId':1, 'title':'untitled'}
 *
 */
router.get(
    '/album/:albumId',
    celebrate(albumSchema.getAlbum),
    sortingCtrl.getAlbum
);

/**
 * @swagger
 * /album:
 *  post:
 *   summary:  Add a new Album
 *   requestBody:
 *     description: album id is system generated Id by mongodb
 *     required: true,
 *     content:
 *      application/json:
 *       schema:
 *        $ref: '#/components/schemas/Album'
 *   responses:
 *    '200':
 *     description: OK
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         message:
 *          type: string
 *          example: successfully created the album
 *         data:
 *          type: object
 *          example: {'sortedList':[{'id':1231, 'index':0},{'id':213, 'index':1}], 'ownerId':1, 'title':'untitled'}
 *
 */
router.post(
    '/album',
    celebrate(albumSchema.createAlbum),
    sortingCtrl.createAlbum
);

/**
 * @swagger
 * /album:
 *  patch:
 *   summary:  Update an Album with new order
 *   requestBody:
 *     required: true,
 *     content:
 *      application/json:
 *       schema:
 *        $ref: '#/components/schemas/Album'
 *   responses:
 *    '200':
 *     description: OK
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         message:
 *          type: string
 *          example: successfully updated the order of the album
 *         data:
 *          type: object
 *          example: {'sortedList':[{'id':1231, 'index':0},{'id':213, 'index':1}], 'ownerId':1, 'title':'untitled'}
 *
 */
router.patch(
    '/album',
    celebrate(albumSchema.updatePhotoOrder),
    sortingCtrl.updateAlbumOrder
);
/**
 * @swagger
 * /album/{albumId}:
 *  delete:
 *   summary:  delete the specified album
 *   parameters:
 *    - in: path
 *      name: albumId
 *      required: true
 *      type: ObjectId
 *      description: album id is system generated Id by mongodb
 *   requestBody:
 *     required: false
 *   responses:
 *    '200':
 *     description: OK
 *     content:
 *      application/json:
 *       schema:
 *        type: object
 *        properties:
 *         message:
 *          type: string
 *          example: successfully updated the order of the album
 *         data:
 *          type: object
 *          example: {'sortedList':[{'id':1231, 'index':0},{'id':213, 'index':1}], 'ownerId':1, 'title':'untitled'}
 *
 */
router.delete(
    '/album/:albumId',
    celebrate(albumSchema.deleteAlbum),
    sortingCtrl.deleteAlbum
);

export default router;
