/**
 * @file sorting service
 * @author Sahan Ridma
 * @description all the services for sorting functionality is written here.
 * all the third-party service/ proxy classes can be inject in this layer
 */
import Album from '../models/album';

function createAlbum(payload) {
    return new Album(payload).save();
}

function updateSortedList({ albumId, sortedList, ownerId }) {
    return Album.findOneAndUpdate({ _id: albumId, ownerId }, { sortedList });
}

function getAlbum({ ownerId = 1, albumId }) {
    return Album.findOne({ ownerId, _id: albumId });
}

function deleteAlbum({ ownerId = 1, albumId }) {
    return Album.findOneAndRemove({ ownerId, _id: albumId });
}
export default { createAlbum, getAlbum, updateSortedList, deleteAlbum };
