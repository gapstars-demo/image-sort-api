/**
 * @file handles album related actions
 * @description controller layer
 * @author Sahan Ridma
 */
import sortingService from '../services/album';

export default class ImageSort {

    async createAlbum(req, res) {
        try {
            const {
                body: { sortedList = [], ownerId = 1, title = 'untitled' }
            } = req;
            const data = await sortingService.save({ ownerId, sortedList, title });
            return res
                .status(200)
                .json({ message: 'successfully saved your image list!', data });
        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        }
    }

    async updateAlbumOrder(req, res) {
        try {
            const {
                body: { sortedList = [], ownerId = 1, albumId }
            } = req;
            const data = await sortingService.updateSortedList({ ownerId, sortedList, albumId });
            return res
                .status(200)
                .json({ message: 'successfully updated your photo order!', data });
        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        }
    }

    async getAlbum(req, res) {
        try {
            const { params: { albumId } } = req;
            const ownerId = 1;
            const data = await sortingService.getAlbum({ownerId, albumId});
            return res.status(200).json({
                message: 'successfully fetched your image list!',
                data
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        }
    }

    async deleteAlbum(req, res) {
        try {
            const { params: { albumId } } = req;
            const ownerId = 1;
            const data = await sortingService.deleteAlbum({ownerId,albumId});
            return res.status(200).json({
                message: 'successfully fetched your image list!',
                data
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(error);
        }
    }
}
